module.exports = {
    root: true,
    env: {
      node: true,
      jasmine: true
    },
    parser: '@typescript-eslint/parser',
    parserOptions:  {
      ecmaVersion:  2018,
      sourceType:  'module',
    },
    extends: [
      'eslint:recommended',
      "plugin:prettier/recommended"
    ],
    rules: {
      "semi": [2, "never"],
      "prettier/prettier": ["error", { "semi": false, "singleQuote": true }]
    }
  }
  