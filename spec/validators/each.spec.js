import each from '@/validators/each'

const itemConstraints = {
  presence: {
    allowEmpty: false,
    message: 'El valor no puede ser nulo'
  },
  numericality: {
    message: 'El valor debe ser numérico'
  }
}

describe("El validador 'each'", function() {
  it('devuelve undefined si no hay errores', function() {
    const model = [1, 2, 3]

    expect(each(model, itemConstraints)).toEqual(undefined)
  })

  it('invoca al validador para cada elemento', function() {
    const model = [1, 'a2', 3, null]

    expect(each(model, itemConstraints)).toEqual({
      1: {
        numericality: 'El valor debe ser numérico'
      },
      3: {
        presence: 'El valor no puede ser nulo'
      }
    })
  })
})
