import properties from '@/validators/properties'

const propertyConstraints = {
  nombre: {
    presence: {
      allowEmpty: false,
      message: 'Debe especificar el nombre'
    }
  },
  apellido: {
    presence: {
      allowEmpty: false,
      message: 'Debe especificar el apellido'
    }
  }
}

describe("El validador 'properties'", function() {
  it('devuelve undefined si no hay errores', function() {
    const model = {
      nombre: 'Germán',
      apellido: 'Cáseres'
    }

    expect(properties(model, propertyConstraints)).toEqual(undefined)
  })

  it('invoca al validador para cada propiedad', function() {
    const model = {
      nombre: '',
      apellido: ''
    }

    expect(properties(model, propertyConstraints)).toEqual({
      nombre: {
        presence: 'Debe especificar el nombre'
      },
      apellido: {
        presence: 'Debe especificar el apellido'
      }
    })
  })
})
