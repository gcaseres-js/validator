import formatter from '@/formatters/formatter'

describe('El formatter', function() {
  it('devuelve una estructura mínima si no hay errores', function() {
    const result = formatter([])

    expect(result).toEqual({})
  })

  it('genera estructuras para resultados de validadores con resultados simples', function() {
    const result = formatter([
      {
        attribute: 'nombre',
        validator: 'presence',
        error: 'El valor es requerido'
      },
      {
        attribute: 'nombre',
        validator: 'mayusculas',
        error: 'El valor debe estar en mayúsculas'
      }
    ])

    expect(result).toEqual({
      nombre: {
        presence: 'El valor es requerido',
        mayusculas: 'El valor debe estar en mayúsculas'
      }
    })
  })

  it('genera estructuras separadas para cada atributo', function() {
    const result = formatter([
      {
        attribute: 'nombre',
        validator: 'presence',
        error: 'El valor es requerido'
      },
      {
        attribute: 'apellido',
        validator: 'presence',
        error: 'El valor es requerido'
      }
    ])

    expect(result).toEqual({
      nombre: {
        presence: 'El valor es requerido'
      },
      apellido: {
        presence: 'El valor es requerido'
      }
    })
  })

  it('genera estructuras anidadas para validadores con resultados compuestos', function() {
    const result = formatter([
      {
        attribute: 'identificacion',
        validator: 'properties',
        error: [
          {
            attribute: 'tipo',
            validator: 'presence',
            error: 'El valor es requerido'
          },
          {
            attribute: 'numero',
            validator: 'presence',
            error: 'El valor es requerido'
          }
        ]
      }
    ])

    expect(result).toEqual({
      identificacion: {
        properties: {
          tipo: {
            presence: 'El valor es requerido'
          },
          numero: {
            presence: 'El valor es requerido'
          }
        }
      }
    })
  })
})
