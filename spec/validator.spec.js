import * as validator from '@/validator'

describe('El validator', function() {
  it('devuelve valid true si no hay errores', function() {
    const model = {}
    const constraints = {
      presence: true
    }

    expect(validator.validate(model, constraints, {}).valid).toBeTruthy()
  })

  it('puede validar constraints sobre el mismo model', function() {
    const model = null
    const constraints = {
      presence: {
        message: 'El model no puede ser nulo'
      }
    }

    expect(validator.validate(model, constraints, {})).toEqual({
      valid: false,
      messages: ['El model no puede ser nulo'],
      children: {}
    })
  })

  it('reconoce el validador properties', function() {
    const model = {
      nombre: 'Germán',
      apellido: ''
    }

    const constraints = {
      properties: {
        nombre: {
          presence: {
            allowEmpty: false,
            message: 'El nombre es requerido'
          }
        },
        apellido: {
          presence: {
            allowEmpty: false,
            message: 'El apellido es requerido'
          }
        }
      }
    }

    expect(validator.validate(model, constraints, {})).toEqual({
      valid: false,
      messages: [],
      children: {
        nombre: {
          valid: true,
          messages: [],
          children: {}
        },
        apellido: {
          valid: false,
          messages: ['El apellido es requerido'],
          children: {}
        }
      }
    })
  })

  it('reconoce el validador each', function() {
    const model = [
      {
        nombre: 'Germán',
        apellido: ''
      },
      {
        nombre: 'Leandro',
        apellido: 'Cáseres'
      }
    ]

    const constraints = {
      each: {
        properties: {
          nombre: {
            presence: {
              allowEmpty: false,
              message: 'El nombre es requerido'
            }
          },
          apellido: {
            presence: {
              allowEmpty: false,
              message: 'El apellido es requerido'
            }
          }
        }
      }
    }

    expect(validator.validate(model, constraints, {})).toEqual({
      valid: false,
      messages: [],
      children: {
        0: {
          valid: false,
          messages: [],
          children: {
            nombre: {
              valid: true,
              messages: [],
              children: {}
            },
            apellido: {
              valid: false,
              messages: ['El apellido es requerido'],
              children: {}
            }
          }
        },
        1: {
          valid: true,
          messages: [],
          children: {
            nombre: {
              valid: true,
              messages: [],
              children: {}
            },
            apellido: {
              valid: true,
              messages: [],
              children: {}
            }
          }
        }
      }
    })
  })
})
