import * as builder from 'src/results/builder'

describe('El Validation Result Builder', function() {
  it('mantiene la estructura del model si no hay errores', function() {
    const validationErrors = undefined
    const emptyObjectModel = {}

    expect(builder.build(validationErrors, emptyObjectModel)).toEqual({
      valid: true,
      messages: [],
      children: {}
    })

    const simpleModel = {
      attribute1: '',
      attribute2: ''
    }

    expect(builder.build(validationErrors, simpleModel)).toEqual({
      valid: true,
      messages: [],
      children: {
        attribute1: {
          valid: true,
          messages: [],
          children: {}
        },
        attribute2: {
          valid: true,
          messages: [],
          children: {}
        }
      }
    })
  })

  it('construye un arreglo de mensajes de error para cada error de los validadores de igual nivel', function() {
    const validationErrors = {
      validator_1: 'Mensaje de error 1',
      validator_2: 'Mensaje de error 2'
    }
    const model = undefined

    expect(builder.build(validationErrors, model)).toEqual({
      valid: false,
      messages: ['Mensaje de error 1', 'Mensaje de error 2'],
      children: {}
    })
  })

  it('establece las propiedades valid y messages para un model inválido con atributos', function() {
    const validationErrors = {
      properties: {
        nombre: {
          presence: 'El valor es requerido'
        }
      }
    }
    const model = {
      nombre: ''
    }

    expect(builder.build(validationErrors, model)).toEqual({
      valid: false,
      messages: [],
      children: {
        nombre: {
          valid: false,
          messages: ['El valor es requerido'],
          children: {}
        }
      }
    })
  })

  it('mantiene la estructura de un model parcialmente inválido con atributos', function() {
    const validationErrors = {
      properties: {
        nombre: {
          presence: 'El valor es requerido'
        }
      }
    }
    const model = {
      nombre: '',
      apellido: 'Cáseres'
    }

    expect(builder.build(validationErrors, model)).toEqual({
      valid: false,
      messages: [],
      children: {
        nombre: {
          valid: false,
          messages: ['El valor es requerido'],
          children: {}
        },
        apellido: {
          valid: true,
          messages: [],
          children: {}
        }
      }
    })
  })

  it('soporta un arreglo como modelo', function() {
    const validationErrors = {
      each: {
        0: {
          properties: {
            nombre: {
              presence: 'El valor es requerido'
            }
          }
        }
      }
    }
    const model = [
      {
        nombre: '',
        apellido: 'Cáseres'
      },
      {
        nombre: 'Leandro',
        apellido: 'Cáseres'
      }
    ]

    expect(builder.build(validationErrors, model)).toEqual({
      valid: false,
      messages: [],
      children: {
        0: {
          valid: false,
          messages: [],
          children: {
            nombre: {
              valid: false,
              messages: ['El valor es requerido'],
              children: {}
            },
            apellido: {
              valid: true,
              messages: [],
              children: {}
            }
          }
        },
        1: {
          valid: true,
          messages: [],
          children: {
            nombre: {
              valid: true,
              messages: [],
              children: {}
            },
            apellido: {
              valid: true,
              messages: [],
              children: {}
            }
          }
        }
      }
    })
  })
})
