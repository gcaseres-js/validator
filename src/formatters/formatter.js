/**
 * @typedef AttributeValidationError
 * @type {Object<string,string|ValidationErrors>}
 *
 * @typedef {Object<string,AttributeValidationError>} ValidationErrors
 */

/**
 * @typedef {Object} ValidateJsRawValidationData
 * @property {String} ValidateJsRawValidationData.attribute
 * @property {String} ValidateJsRawValidationData.validator
 * @property {String|ValidateJsRawValidationData[]} ValidateJsRawValidationData.error
 *
 * @param {ValidateJsRawValidationData[]} errors
 * @returns {ValidationErrors}
 */
function formatter(errors) {
  return errors.reduce((formatted, error) => {
    if (!formatted[error.attribute]) {
      formatted[error.attribute] = {}
    }
    if (error.error instanceof Array) {
      formatted[error.attribute][error.validator] = formatter(error.error)
    } else {
      formatted[error.attribute][error.validator] = error.error
    }

    return formatted
  }, {})
}

export default formatter
