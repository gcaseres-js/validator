import * as _ from 'lodash'
import * as validatejs from 'validate.js'
import properties from './validators/properties'
import each from './validators/each'
import formatter from './formatters/formatter'
import * as results from './results/builder'

validatejs.formatters['gcaseres'] = formatter
validatejs.validators['properties'] = properties
validatejs.validators['each'] = each

/**
 * @typedef {Object} ValidatorOptions
 * @property {Boolean} [buildResults]
 * @property {import('validate.js').ValidateOption} [ValidatorOptions.validatejs]
 */

/**
 *
 * @param {any} model
 * @param {any} constraints
 * @param {ValidatorOptions} [options]
 * @returns {import('./results/builder').ValidationResults}
 */
export function validate(model, constraints, options) {
  options = _.defaultsDeep(options, {
    buildResults: true,
    validatejs: {
      fullMessages: false,
      format: 'gcaseres'
    }
  })

  const errors = validatejs.validate(
    { model: model },
    { model: constraints },
    options.validatejs
  )

  if (options.buildResults) {
    if (errors) {
      return results.build(errors.model, model)
    } else {
      return results.build(errors, model)
    }
  } else {
    if (errors) {
      return errors.model
    } else {
      return undefined
    }
  }
}
