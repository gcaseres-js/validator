import * as validator from '../validator'

/**
 * @param {any[]} items
 * @param {object} itemConstraints
 * @returns {undefined|object}
 */
function eachValidator(items, itemConstraints) {
  items = items || []
  return items.reduce(
    /** @type {{ (itemErrorsMap: any, item: any, itemIndex: Number): any }} */
    (itemErrorsMap, item, itemIndex) => {
      const itemErrors = validator.validate(item, itemConstraints, {
        buildResults: false
      })

      if (itemErrors) {
        if (itemErrorsMap === undefined) {
          itemErrorsMap = {}
        }
        itemErrorsMap[itemIndex] = itemErrors
      }

      return itemErrorsMap
    },
    undefined
  )
}

export default eachValidator
