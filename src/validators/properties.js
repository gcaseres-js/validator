import * as validator from '../validator'

/**
 * @param {object} object
 * @param {object} propertiesConstraints
 * @returns {undefined|object}
 */
function propertiesValidator(object, propertiesConstraints) {
  object = object || {}
  return Object.keys(propertiesConstraints).reduce(
    /** @type {{ (propertyErrorsMap: any, property: string): any }} */
    (propertyErrorsMap, property) => {
      const propertyErrors = validator.validate(
        object[property],
        propertiesConstraints[property],
        {
          buildResults: false
        }
      )

      if (propertyErrors) {
        if (propertyErrorsMap === undefined) {
          propertyErrorsMap = {}
        }
        propertyErrorsMap[property] = propertyErrors
      }

      return propertyErrorsMap
    },
    undefined
  )
}

export default propertiesValidator
