import * as _ from 'lodash'

/**
 * @typedef {import('src/formatters/formatter').AttributeValidationError} ValidationErrors
 */

/**
 * @typedef {Object} ValidationResults
 * @property {Boolean} ValidationResults.valid False si existen errores de validación en el nivel actual o en algún hijo.
 * @property {string[]} ValidationResults.messages Mensajes de error del nivel actual.
 * @property {Object<string,ValidationResults>} ValidationResults.children Validadores anidados.
 */

/**
 * Genera resultados de validación a partir de un conjunto de errores de validación.
 *
 * Los resultados de validación mantienen la estructura del modelo que se está validando
 * aunque el mismo no contenga errores.
 * En caso que existan errores, se establece el atributo valid en false y se incluyen los
 * mensajes de error en el arreglo messages.
 *
 * @param {ValidationErrors} validationErrors
 * @param {any} model
 * @returns {ValidationResults}
 */
export function build(validationErrors, model) {
  return _.defaultsDeep(
    buildValidationResults(validationErrors || {}),
    buildDefaults(model)
  )
}

/**
 * @typedef {{ (validationErrors: ValidationErrors): ValidationResults }} ValidationResultsBuilder
 * @type {Object<string, ValidationResultsBuilder>}
 */
const validatorResultBuilderMap = {
  properties: validationErrors => {
    return {
      valid: false,
      messages: [],
      children: Object.keys(validationErrors).reduce(
        (validationResults, property) => {
          validationResults[property] = buildValidationResults(
            validationErrors[property]
          )
          return validationResults
        },
        {}
      )
    }
  },
  each: validationErrors => {
    return {
      valid: false,
      messages: [],
      children: Object.keys(validationErrors).reduce(
        (validationResults, index) => {
          validationResults[index] = buildValidationResults(
            validationErrors[index]
          )
          return validationResults
        },
        {}
      )
    }
  }
}

/**
 *
 * @param {ValidationErrors} validationErrors
 * @returns {ValidationResults}
 */
function fallbackBuildValidationResults(validationErrors) {
  if (typeof validationErrors !== 'string') {
    throw new Error(
      `El fallback es incapaz de procesar el tipo de error de validación obtenido`
    )
  }
  return {
    valid: false,
    messages: [validationErrors],
    children: {}
  }
}

/**
 * @param {ValidationErrors} validationErrors
 * @returns {ValidationResultsBuilder}
 */
function buildValidationResults(validationErrors) {
  return _.mergeWith(
    {},
    ...Object.keys(validationErrors).reduce((validationResults, validator) => {
      let currentValidationResults
      if (validatorResultBuilderMap[validator] !== undefined) {
        currentValidationResults = validatorResultBuilderMap[validator](
          validationErrors[validator]
        )
      } else {
        currentValidationResults = fallbackBuildValidationResults(
          validationErrors[validator]
        )
      }

      validationResults.push(currentValidationResults)
      return validationResults
    }, []),
    (objValue, srcValue) => {
      if (_.isArray(objValue)) {
        return objValue.concat(srcValue)
      }
    }
  )
}

/**
 * Genera resultados genéricos válidos para el model y todos sus atributos
 * @param {any} model
 * @returns {ValidationResults}
 */
function buildDefaults(model) {
  model = model || {}
  if (typeof model !== 'object' || typeof model === 'string') {
    return {
      valid: true,
      messages: [],
      children: {}
    }
  } else {
    return {
      valid: true,
      messages: [],
      children: Object.keys(model).reduce((defaults, property) => {
        defaults[property] = buildDefaults(model[property])
        return defaults
      }, {})
    }
  }
}
